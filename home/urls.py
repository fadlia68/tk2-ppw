from django.urls import include, path
from . import views

app_name = 'home'

urlpatterns = [
    path('', views.index, name='index'),
    path('register/', views.register, name = 'register'),
    path('logout/', views.view_logout, name = 'logout')
]
