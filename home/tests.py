from django.test import TestCase, Client
from django.contrib.auth.models import User
from .forms import RegisForm

# Create your tests here.
class TestAuthentication(TestCase):
    
    def test_if_register_url_is_exist(self):
        response = Client().get('/register/')
        self.assertEqual(response.status_code, 200)

    def test_if_register_html_is_exist(self):
        response = Client().get('/register/')
        self.assertTemplateUsed(response, 'register.html')

    def test_if_home_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_if_home_html_is_exist(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_register_someone_success(self):
        form_data_user = {
            'username' : 'raditya',
            'password1' : 'inipasswordnya',
            'password2' : 'inipasswordnya',
        }

        response = Client().post('/register/', data = form_data_user)
        self.assertEqual(response.status_code, 200)

        html_response = response.content.decode('utf-8')
        self.assertIn('Congratulations, your account has been successfully created! Please Kindly Login on the Login Button', html_response)

    def test_register_someone_failed(self):
        form_data_user = {
            'username' : 'raditya',
            'password1' : 'inipasswordnya',
            'password2' : 'inipasswordnyaa',
        }

        response = Client().post('/register/', data = form_data_user)
        self.assertEqual(response.status_code, 200)

        html_response = response.content.decode('utf-8')
        self.assertIn('ERROR: Something went wrong. User Registration failed', html_response)

    def test_log_in_someone_success(self):
        form_data_user = {
            'username' : 'raditya',
            'password1' : 'inipasswordnya',
            'password2' : 'inipasswordnya',
        }

        Client().post('/register/', data = form_data_user)

        user_login = {
            'username' : 'raditya',
            'password' : 'inipasswordnya',
        }

        response = Client().post('/', user_login)

        html_response = response.content.decode('utf-8')
        self.assertNotIn('Please LogIn Here!', html_response)

    def test_log_in_someone_failed(self):
        form_data_user = {
            'username' : 'raditya',
            'password1' : 'inipasswordnya',
            'password2' : 'inipasswordnya',
        }

        Client().post('/register/', data = form_data_user)

        user_login = {
            'username' : 'raditya',
            'password' : 'inipasswordnyaa',
        }

        response = Client().post('/', user_login)

        html_response = response.content.decode('utf-8')
        self.assertIn('Username or password not correct', html_response)


    def log_out(self):
        form_data_user = {
            'username' : 'raditya',
            'password1' : 'inipasswordnya',
            'password2' : 'inipasswordnya',
        }

        Client().post('/register/', data = form_data_user)

        user_login = {
            'username' : 'raditya',
            'password' : 'inipasswordnya',
        }

        Client().post('/', user_login)

        response = Client().get('/logout/')

        html_response = response.content.decode('utf-8')
        self.assertIn('Please LogIn Here!', html_response)
        