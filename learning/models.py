from django.db import models

# Create your models here.
class Category(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

class Subject(models.Model):
    name = models.CharField(max_length=100, null=True)
    category = models.ManyToManyField(Category, related_name='subjects')
    description = models.TextField(max_length=400, null=True)
    url = models.URLField(null=True)
    image = models.URLField(null=True, blank=True)

    def __str__(self):
        return self.name