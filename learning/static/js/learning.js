$(document).ready(function() {
    console.log("js");
    if($('.random').hasClass('first')) {
        // init awal munculin data
        $(this).removeClass('first');
        $.ajax({
            url: '/learning/postr',
            success: function(data) {
                func(data, '.row.rowrandom');
            },
        })
        $.ajax({
            url: '/learning/post',
            success: function(data) {
                func(data, '.row.all');
            },
        })
    }

    if($('.row').hasClass('more')) {
        // untuk more.html
        query = $('.query').text().toLowerCase();
        $.ajax({
            url: '/learning/postc/' + query,
            success: function(data) {
                func(data, '.row.more');
            },
        })
    }

    $('.more-btn').click(function() {
        // ubah2 preview - view all
        console.log("more button clicked")
        if(!$(this).hasClass('more')) {
            // preview --> all
            $('.all-body').slideDown('slow').addClass('active');
            $('.row.all').removeClass('nonactive');
            $('.note').empty();
            $('#search-source').val('');
            $('.row.search').empty();
            $('.random').slideUp('slow').addClass('nonactive');
            $(this).text('Preview');
            $('.bag-2').text('All Sources');
            $(this).addClass('more');
        } else {
            // all -> preview
            $('.all-body').slideUp('slow').removeClass('active');
            $('.random').slideDown('slow').removeClass('nonactive');
            $(this).text('View All');
            $('.bag-2').text('Preview');
            $(this).removeClass('more');
        }

    });

    $('.row').on('click', '#read-more', function() {
        // read more description
        if($(this).hasClass('active')) {
            // less --> more
            $(this).text('Read More');
            $(this).removeClass('active');
            $(this).parent().siblings('.desc-more').removeClass('active');
        } else {
            // more --> less
            $(this).text('Read Less');
            $(this).addClass('active');
            $(this).parent().siblings('.desc-more').addClass('active');
        }
    });

    $('#search-source').keyup(function() {
        // fitur search
        console.log("searching..");
        var keyword = $(this).val();

        $('.row.search').empty();
        $('.row.all').addClass('nonactive');
        $('.note').empty();
        $('.note').append('Searching...');

        if(keyword == "") {
            $('.note').empty();
            $('.note').append('Waiting for you to type above⏳');
        } else {

            $.ajax({
                url: "/learning/post/" + keyword,
                success: function(data) {

                    $('.note').empty();

                    if(data.length == 0) {
                        $('.note').append('No result found. Find another source🔍');
                    } else {
                        func(data, '.row.search');
                    }

                }
            })
        }
    })

    $('.add-source-btn').click(function() {
        // mau munculin form
        console.log("add button clicked");
        $(this).addClass('nonactive');
        $('.add-source').removeClass('nonactive');
        $('.add-source-btn-back').addClass('nonactive');
        $('.note2').text('');
    })

    $('.study-form').on('submit', function(e) {
        // form nya disubmit
        e.preventDefault();
        var checkbox_value = [];
        $(":checkbox").each(function () {
            var ischecked = $(this).is(":checked");
            if (ischecked) {
                checkbox_value.push($(this).val());
            }
        });
        var namanya = $('#name').val();

        $.ajax({
            url: '/learning/create_post/',
            type: 'POST',
            data: {
                nama: $('#name').val(),
                desc: $('#desc').val(),
                category: JSON.stringify(checkbox_value),
                url: $('#url').val(),
                image: $('#image').val(),
                csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val(),
            },

            success: function(data) {
                $(":checkbox").each(function () {
                    $(this).prop('checked', false);
                });
                $('#name').val('');
                $('#desc').val('');
                $('#url').val('');
                $('#image').val('');
                $('.note2').text(namanya + ' has been posted!');
                $('.add-source-btn').removeClass('nonactive');
                $('.add-source').addClass('nonactive');
                $('.add-source-btn-back').removeClass('nonactive');
                func2(data, '.row.all');

            },

            error: function() {
                alert("I'm sorry, something went wrong");
            }
        })
    })

    function func(data, row) {
        // append ke html untuk successnya ajax
        for(var i = 0; i < data.length; i++) {
            nama = data[i].fields.name;
            category = data[i].fields.category;
            cat0 = '<a href="/learning/category/'
            cat1 = '" class="btn button-learning p-2">';
            for(var j = 0; j < category.length; j++) {
                category[j] = category[j] == 1 ? cat0 + "programming/" + cat1 + "programming</a>" : 
                    category[j] == 2 ? cat0 + "math/" + cat1 + "math</a>" :
                    category[j] == 3 ? cat0 + "science/" + cat1 + "science</a>" : 
                    category[j] == 4 ? cat0 + "language/" + cat1 + "language</a>" :
                    category[j] == 5 ? cat0 + "business/" + cat1 + "business</a>" : 
                    cat0 + "others/" + cat1 + "others</a>";
            }
            description = data[i].fields.description;
            url = data[i].fields.url;
            image = data[i].fields.image != null ? data[i].fields.image : "/static/img/no-image.png";
            $(row).append(
                '<div class="col-sm-4"><div class="card bg-light mb-3" style="border: none;"><div class="card-header" style="background: white;">'+
                '<div><img src="' + image + '" style="border-radius: 50%; height:100px; width: 100px; object-fit: cover; border: solid 2px #B3A394;"></div>'+
                '</div><div class="card-body"><div class="study-source-title">'+ nama+ '</div>'+
                category.join(' ') +
                '<p class="study-source-desc desc-more" style="color: #102542;">'+ description + '</p>'+
                '<div style="text-align: center;" class="study-site">'+
                '<a class="btn submit-button-learning p-2" style="color: white;" id="read-more">Read More</a>'+
                '<a href="'+url+'" target="_blank" class="btn submit-button-learning p-2">Go To Site</a>'+
                '</div></div></div></div>'
            );
        }   
    }

    function func2(data, row) {
        // append html khusus setelah submit form
        nama = data['name'];
        category = data['category'];
        cat0 = '<a href="/learning/category/'
        cat1 = '" class="btn button-learning p-2">';
        for(var j = 0; j < category.length; j++) {
            category[j] = category[j] == 1 ? cat0 + "programming/" + cat1 + "programming</a>" : 
                category[j] == 2 ? cat0 + "math/" + cat1 + "math</a>" :
                category[j] == 3 ? cat0 + "science/" + cat1 + "science</a>" : 
                category[j] == 4 ? cat0 + "language/" + cat1 + "language</a>" :
                category[j] == 5 ? cat0 + "business/" + cat1 + "business</a>" : 
                cat0 + "others/" + cat1 + "others</a>";
        }
        description = data['description'];
        url = data['url'];
        image = data['image'] != '' ? data['image'] : "/static/img/no-image.png";
        $(row).append(
            '<div class="col-sm-4"><div class="card bg-light mb-3" style="border: none;"><div class="card-header" style="background: white;">'+
            '<div><img src="' + image + '" style="border-radius: 50%; height:100px; width: 100px; object-fit: cover; border: solid 2px #B3A394;"></div>'+
            '</div><div class="card-body"><div class="study-source-title">'+ nama+ '</div>'+
            category.join(' ') +
            '<p class="study-source-desc desc-more" style="color: #102542;">'+ description + '</p>'+
            '<div style="text-align: center;" class="study-site">'+
            '<a class="btn submit-button-learning p-2" style="color: white;" id="read-more">Read More</a>'+
            '<a href="'+url+'" target="_blank" class="btn submit-button-learning p-2">Go To Site</a>'+
            '</div></div></div></div>'
        );   
    }
})