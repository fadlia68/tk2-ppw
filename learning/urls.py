from django.urls import include, path
from . import views

app_name = 'learning'

urlpatterns = [
    path('', views.index, name='index'),
    path('category/<str:ex>/', views.explore, name='explore'),
    path('create_post/', views.create_post, name='create_post'),
    path('post/<str:cat>', views.post, name='post'),
    path('postc/<str:cat>', views.postc, name='postc'),
    path('postr/', views.postR, name='postR'),
    path('post/', views.postAll, name='postAll'),
]
