from django.test import TestCase, Client
from .models import Category, Subject
from django.urls import reverse
from .forms import SubjectForm
from django.contrib.auth.models import User

# Create your tests here.
class Testing(TestCase):
    def setUp(self):
        self.client = Client()
        self.pro = Category.objects.create(name="programming")
        Category.objects.create(name="math")
        self.sub1 = Subject.objects.create(name="sub1")
        Subject.objects.create(name="sub2")
        Subject.objects.create(name="sub3")

    def test_url_learning_exist(self):
        response = Client().get('/learning/')
        self.assertEqual(response.status_code, 200)

    def test_nama_model_category(self):
        self.assertEqual(str(self.pro), 'programming')

    def test_nama_model_subject(self):
        self.assertEqual(str(self.sub1), 'sub1')

    def test_apakah_model_subject_ada(self):
        hitung_berapa = Subject.objects.all().count()
        self.assertEquals(hitung_berapa, 3)

    def test_explore_url(self):
        response = Client().get('/learning/category/programming/')
        self.assertEqual(response.status_code, 200)

    def test_explore_url_gaada(self):
        response = Client().get('/learning/category/gaada/')
        self.assertEqual(response.status_code, 302)

    def test_json_post(self):
        response = Client().get('/learning/post/aaaaaaaaaaaaaaaaaa')
        self.assertJSONEqual(response.content.decode("utf-8"), [])
    
    def test_addSource_success(self):
        response2 = Client().get('/learning/create_post/')
        self.assertEqual(response2.status_code, 200)

    def test_create_post(self):
        data = {
            'nama' : 'namanya',
            'category' : '[1]',
            'description' : 'yagitu',
            'url' : 'https://www.google.co.id',
            'image' : '',
        }
        response = Client().post('/learning/create_post/', data)
        self.assertEqual(response.status_code, 200)

    def test_create_post2(self):
        data = {
            'nama' : 'namanya',
            'category' : '[1]',
            'description' : 'yagitu',
            'url' : 'https://www.google.co.id',
            'image' : 'https://upload.wikimedia.org/wikipedia/commons/thumb/3/30/Googlelogo.png/1199px-Googlelogo.png',
        }
        response = Client().post('/learning/create_post/', data)
        self.assertEqual(response.status_code, 200)

    def test_postr(self):
        response = Client().get('/learning/postr/')
        self.assertEqual(response.status_code, 200)    

    def test_postAll(self):
        response = Client().get('/learning/post/')
        self.assertEqual(response.status_code, 200)

    def test_postc(self):
        response = Client().get('/learning/postc/math')
        self.assertEqual(response.status_code, 200)
