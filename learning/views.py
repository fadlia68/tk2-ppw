from django.shortcuts import render, redirect
from .models import *
from .forms import *
import random
from django.core import serializers
from django.http import JsonResponse
import json

# Create your views here.

def index(request):
    form = SubjectForm()
    context = {'form' : form}
    return render(request, 'main/learning.html', context)

def create_post(request):
    response_data = {}
    
    if request.method == 'POST':
        name = request.POST.get('nama')
        category = json.loads(request.POST.get('category'))
        description = request.POST.get('desc')
        url = request.POST.get('url')
        image = request.POST.get('image')

        response_data['name'] = name
        response_data['category'] = category
        response_data['description'] = description
        response_data['url'] = url
        response_data['image'] = image

        instance = Subject.objects.create(
            name=name, description=description, url=url,
        )

        if(image != ''):
            instance.image = image
            instance.save()

        instance.category.set(category)

        return JsonResponse(response_data)

    return JsonResponse({'success' : False})

def explore(request, ex):
    try:
        category = Category.objects.get(name=ex)
    except:
        return redirect('/learning')
    form = SubjectForm()
    context = {'query' : ex.capitalize(), 'form' : form}
    return render(request, 'main/more.html', context)

def post(request, cat):
    subject = Subject.objects.filter(name__icontains=cat)
    post_c = serializers.serialize('json', subject)
    data = json.loads(post_c)
    return JsonResponse(data, safe=False)
    
def postR(request):
    subject = set(Subject.objects.all())
    lst = random.sample(subject, 3)
    post_c = serializers.serialize('json', lst)
    data = json.loads(post_c)
    return JsonResponse(data, safe=False)

def postAll(request):
    subject = Subject.objects.all()
    post_c = serializers.serialize('json', subject)
    data = json.loads(post_c)
    return JsonResponse(data, safe=False)

def postc(request, cat):
    category = Category.objects.get(name=cat)
    subject = category.subjects.all()
    post_c = serializers.serialize('json', subject)
    data = json.loads(post_c)
    return JsonResponse(data, safe=False)