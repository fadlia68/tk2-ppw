$(document).ready(function() {
    $('.submit-button').click(function() {
        var data = $('.task-form').serialize();
        
        $.ajax({
            url: '/todo/addTask/',
            timeout: 3000,
            method: 'POST',
            data: data,
            success : function(response) {
                console.log(response.result);
                $(location).attr('href','/todo/');
            }
        });
    });

    $('#personal-tasks').click(function() {
        $.ajax({
            url: '/todo/getPersonalTask/',
            timeout: 3000,
            method: 'GET',
            dataType : 'json',
            success: function(response) {
                $('#list-of-personal-task').empty();
                $('.task-title').text("Personal Tasks for You!")
                $('.button-link').html(
                    '<div class="home-btn"><i class="fa fa-home"></i></div>'
                );
                var result = response.result;
                console.log(result);
                
                for (var i = 0; i < result.length; i++) {
                    $('#list-of-personal-task').append(
                        '<div class="d-flex flex-column task-detail"><div class="d-flex flex-row justify-content-between">' + 
                        '<div class="task-name">' + result[i].title + '</div><div class="task-datetime">' + result[i].time + 
                        '</div></div><div class="d-flex flex-row justify-content-between"><div class="task-description">' + result[i].description + 
                        '</div><div class="trash-btn" data-id="' + result[i].id + '"><i class="fa fa-trash"></i></div></div>' +
                        '<div class="task-datetime">'+ result[i].date +'</div></div>'
                    );
                };
                
            }
        });
    });

    $('#group-tasks').click(function() {
        $.ajax({
            url: '/todo/getGroupTask/',
            timeout: 3000,
            method: 'GET',
            dataType : 'json',
            success: function(response) {
                $('#list-of-personal-task').empty();
                $('.task-title').text("Group Tasks for You!");
                $('.button-link').html(
                    '<div class="home-btn"><i class="fa fa-home"></i></div>'
                );
                var result = response.result;
                console.log(result);
                
                for (var i = 0; i < result.length; i++) {
                    $('#list-of-personal-task').append(
                        '<div class="d-flex flex-column task-detail"><div class="d-flex flex-row justify-content-between">' + 
                        '<div class="task-name">' + result[i].title + '</div><div class="task-datetime">' + result[i].time + 
                        '</div></div><div class="d-flex flex-row justify-content-between"><div class="task-description">' + result[i].description + 
                        '</div><div class="trash-btn" data-id="' + result[i].id + '"><i class="fa fa-trash"></i></div></div>' +
                        '<div class="task-datetime">'+ result[i].date +'</div></div>'
                    );
                };
                
            }
        });
    });

    $(document).on('click', '.home-btn', function() {
        
        $.ajax({
            url: '/todo/getAllTask/',
            timeout: 3000,
            method: 'GET',
            dataType : 'json',
            success: function(response) {
                $('#list-of-personal-task').empty();
                $('.task-title').text("Tasks for You!");
                var result = response.result;
                $('.button-link').html(
                    '<a href="/todo/addTask/"><div class="add-button">+</div></a>'
                );
                console.log(result);
                
                for (var i = 0; i < result.length; i++) {
                    $('#list-of-personal-task').append(
                        '<div class="d-flex flex-column task-detail"><div class="d-flex flex-row justify-content-between">' + 
                        '<div class="task-name">' + result[i].title + '</div><div class="task-datetime">' + result[i].time + 
                        '</div></div><div class="d-flex flex-row justify-content-between"><div class="task-description">' + result[i].description + 
                        '</div><div class="trash-btn" data-id="' + result[i].id + '"><i class="fa fa-trash"></i></div></div>' +
                        '<div class="task-datetime">'+ result[i].date +'</div></div>'
                    );
                };
                
            }
        });  
        
    });

    $(document).on('click', '.trash-btn', function() {
        var data_id = $(this).data('id');

        $.ajax({
            url: 'deleteTask/' + data_id,
            timeout: 3000,
            method: 'GET',
            data: {
                csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val(),
            },
            success : function(response) {
                console.log(response.result);
                $('.trash-btn[data-id="' + data_id + '"]').parent().parent().remove();
            }
        });

    });

    $(document).on('mouseenter', '.task-detail', function() {
        $(this).css('font-weight', '900');
    });

    $(document).on('mouseleave', '.task-detail', function() {
        $(this).css('font-weight', 'normal');
    });
});