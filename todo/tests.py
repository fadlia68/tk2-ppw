from django.test import TestCase, Client
from django.contrib.auth.models import User
from .models import Task

class TestToDo(TestCase):
    def set_up(self):
        self.client = Client()

    def test_if_todo_url_is_exist(self):
        response = self.client.get('/todo/')
        self.assertEqual(response.status_code, 200)

    def test_if_todo_html_is_exist(self):
        response = self.client.get('/todo/')
        self.assertTemplateUsed(response, 'main/todo.html')

    def test_if_todo_content_is_exist_when_user_is_not_logged_in(self):
        response = self.client.get('/todo/')
        html_response = response.content.decode('utf-8')
        self.assertIn("TO DO", html_response)
        self.assertIn("You can easily manage your tasks here.", html_response)
        self.assertIn("Authentication Required", html_response)

    def test_if_todo_content_is_exist_when_user_is_logged_in(self):
        User.objects.create_user(
            username='raditya', 
            password='rahasia'
        )
        self.client.login(username="raditya", password="rahasia")

        response = self.client.get('/todo/')
        html_response = response.content.decode('utf-8')
        self.assertIn("TO DO", html_response)
        self.assertIn("You can easily manage your tasks here.", html_response)
        self.assertIn("Tasks for You!", html_response)
        self.assertIn("Task Category", html_response)

    def test_if_add_task_url_is_exist(self):
        response = self.client.get('/todo/addTask/')
        self.assertEqual(response.status_code, 200)

    def test_if_add_task_html_is_exist(self):
        response = self.client.get('/todo/addTask/')
        self.assertTemplateUsed(response, 'main/add-task.html')

    def test_if_todo_content_is_exist(self):
        response = self.client.get('/todo/addTask/')
        html_response = response.content.decode('utf-8')
        self.assertIn("Create a New Task", html_response)
        self.assertIn("Task Title", html_response)
        self.assertIn("Task Description", html_response)
        self.assertIn("Task Category", html_response)
        self.assertIn("Deadline - Date", html_response)
        self.assertIn("Deadline - Time", html_response)

    def test_models_created(self):
        User.objects.create_user(
            username='raditya', 
            password='rahasia'
        )
        self.client.login(username="raditya", password="rahasia")
        user = User.objects.get(username="raditya")

        task = Task.objects.create(
            title = "TK PPW",
            description = "Tugas Kelompok PPW Tahap 2",
            category = "Group Task",
            date = "2020-11-21",
            time = "12:00:00"
        )
        task.save()
        user.task.add(task)

        all_task = user.task.order_by('date', 'time', 'title')
        self.assertEqual(all_task.count(), 1)

    def test_add_task(self):
        User.objects.create_user(
            username='raditya', 
            password='rahasia'
        )
        self.client.login(username="raditya", password="rahasia")

        task = {
            'title' : 'TK PPW',
            'description' : 'Tugas Kelompok PPW Tahap 2',
            'category' : 'Group Task',
            'date' : '2020-11-21',
            'time' : '22:00:00',
        }
        self.client.post('/todo/addTask/', task)

        response = self.client.get('/todo/')
        html_response = response.content.decode('utf-8')
        self.assertEqual(response.status_code, 200)
        self.assertIn('TK PPW', html_response)
        self.assertIn('Tugas Kelompok PPW Tahap 2', html_response)
        self.assertIn('Nov. 21, 2020', html_response)
        self.assertIn('10 p.m.', html_response)

    def test_get_all_personal_task(self):
        User.objects.create_user(
            username='raditya', 
            password='rahasia'
        )
        self.client.login(username="raditya", password="rahasia")

        task = {
            'title' : 'TK PPW',
            'description' : 'Tugas Kelompok PPW Tahap 2',
            'category' : 'Personal Task',
            'date' : '2020-11-21',
            'time' : '22:00:00',
        }
        response = self.client.get('/todo/getPersonalTask/')
        self.assertEqual(response.status_code, 200)


    def test_get_all_group_task(self):
        User.objects.create_user(
            username='raditya', 
            password='rahasia'
        )
        self.client.login(username="raditya", password="rahasia")

        task = {
            'title' : 'TK PPW',
            'description' : 'Tugas Kelompok PPW Tahap 2',
            'category' : 'Group Task',
            'date' : '2020-11-21',
            'time' : '22:00:00',
        }
        response = self.client.get('/todo/getGroupTask/')
        self.assertEqual(response.status_code, 200)

    def test_get_all_task(self):
        User.objects.create_user(
            username='raditya', 
            password='rahasia'
        )
        self.client.login(username="raditya", password="rahasia")

        task = {
            'title' : 'TK PPW',
            'description' : 'Tugas Kelompok PPW Tahap 2',
            'category' : 'Group Task',
            'date' : '2020-11-21',
            'time' : '22:00:00',
        }
        response = self.client.get('/todo/getAllTask/')
        self.assertEqual(response.status_code, 200)

    def test_delete_task(self):
        User.objects.create_user(
            username='raditya', 
            password='rahasia'
        )
        self.client.login(username="raditya", password="rahasia")
        user = User.objects.get(username="raditya")

        task = Task.objects.create(
            title = "TK PPW",
            description = "Tugas Kelompok PPW Tahap 2",
            category = "Group Task",
            date = "2020-11-21",
            time = "22:00:00"
        )
        task.save()
        user.task.add(task)

        response = self.client.get('/todo/')
        html_response = response.content.decode('utf-8')
        self.assertIn('TK PPW', html_response)

        self.client.get('/todo/deleteTask/%d' % task.id)

        response = self.client.get('/todo/')
        html_response = response.content.decode('utf-8')
        self.assertNotIn('TK PPW', html_response)