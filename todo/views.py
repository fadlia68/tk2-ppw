from django.shortcuts import render, redirect
from django.http import JsonResponse
from .forms import TaskForm
from .models import Task

def index(request):
    if request.user.is_authenticated:
        all_task = request.user.task.order_by('date', 'time', 'title')
    else :
        all_task = []
    return render(request, 'main/todo.html', {'allTask' : all_task})

def addTask(request):
    taskform = TaskForm()
    if request.method == "POST":
        taskForm = TaskForm(request.POST)
        if taskForm.is_valid():
            task = Task(
                title = request.POST['title'],
                description = request.POST['description'],
                category = request.POST['category'],
                date = request.POST['date'],
                time = request.POST['time'],
            )
            task.save()
            request.user.task.add(task)
            return JsonResponse({"result" : "POSTED"})

    return render(request, 'main/add-task.html', {'taskform' : taskform})

def getPersonalTask(request):
    all_task = request.user.task.filter(category = "Personal Task").order_by('date', 'time', 'title').values()
    return JsonResponse({"result" : list(all_task)})

def getGroupTask(request):
    all_task = request.user.task.filter(category = "Group Task").order_by('date', 'time', 'title').values()
    return JsonResponse({"result" : list(all_task)})

def getAllTask(request):
    all_task = request.user.task.order_by('date', 'time', 'title').values()
    return JsonResponse({"result" : list(all_task)})

def deleteTask(request, task_id):
    task = Task.objects.get(id=task_id)
    task.delete()
    return JsonResponse({"result" : "Task has been deleted"})