from django.urls import include, path
from . import views

app_name = 'todo'

urlpatterns = [
    path('', views.index, name='index'),
    path('addTask/',  views.addTask, name='addTask'),
    path('getPersonalTask/', views.getPersonalTask, name = 'getPersonalTask'),
    path('getGroupTask/', views.getGroupTask, name = 'getGroupTask'),
    path('getAllTask/', views.getAllTask, name = 'getAllTask'),
    path('deleteTask/<int:task_id>', views.deleteTask, name='deleteTask'),
]
