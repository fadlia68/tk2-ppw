from django.db import models

class Article(models.Model):
    name = models.CharField(max_length=100)
    title = models.CharField(max_length=100)
    foto = models.CharField(max_length=100, null=True)
    keyword = models.TextField()

    def __str__(self):
        return self.name

class Comment(models.Model):
    author = models.CharField(max_length=100, default='ayukasnht')
    content = models.TextField()
    email = models.EmailField(default='sonohataayuka@gmail.com')

    def __str__(self):
        return self.author
    
    def get_dict(self):
        return {
            'author':self.author,
            'content':self.content,
            'email':self.email,
            'id':self.pk
        }

class CommentKdrama(models.Model):
    author = models.CharField(max_length=100, default='DEFAULT VALUE')
    content = models.TextField()
    email = models.EmailField(default='DEFAULT VALUE')

    def __str__(self):
        return self.author
    
    def get_dict(self):
        return {
            'author':self.author,
            'content':self.content,
            'email':self.email,
            'id':self.pk
        }

class CommentAnime(models.Model):
    author = models.CharField(max_length=100, default='DEFAULT VALUE')
    content = models.TextField()
    email = models.EmailField(default='DEFAULT VALUE')

    def __str__(self):
        return self.author
    
    def get_dict(self):
        return {
            'author':self.author,
            'content':self.content,
            'email':self.email,
            'id':self.pk
        }

class CommentRomantic(models.Model):
    author = models.CharField(max_length=100, default='DEFAULT VALUE')
    content = models.TextField()
    email = models.EmailField(default='DEFAULT VALUE')

    def __str__(self):
        return self.author
    
    def get_dict(self):
        return {
            'author':self.author,
            'content':self.content,
            'email':self.email,
            'id':self.pk
        }

class CommentKpop(models.Model):
    author = models.CharField(max_length=100, default='DEFAULT VALUE')
    content = models.TextField()
    email = models.EmailField(default='DEFAULT VALUE')

    def __str__(self):
        return self.author
    
    def get_dict(self):
        return {
            'author':self.author,
            'content':self.content,
            'email':self.email,
            'id':self.pk
        }

class CommentPodcast(models.Model):
    author = models.CharField(max_length=100, default='DEFAULT VALUE')
    content = models.TextField()
    email = models.EmailField(default='DEFAULT VALUE')

    def __str__(self):
        return self.author
    
    def get_dict(self):
        return {
            'author':self.author,
            'content':self.content,
            'email':self.email,
            'id':self.pk
        }

class CommentTiktok(models.Model):
    author = models.CharField(max_length=100, default='DEFAULT VALUE')
    content = models.TextField()
    email = models.EmailField(default='DEFAULT VALUE')

    def __str__(self):
        return self.author
    
    def get_dict(self):
        return {
            'author':self.author,
            'content':self.content,
            'email':self.email,
            'id':self.pk
        }

class CommentGames1(models.Model):
    author = models.CharField(max_length=100, default='DEFAULT VALUE')
    content = models.TextField()
    email = models.EmailField(default='DEFAULT VALUE')

    def __str__(self):
        return self.author
    
    def get_dict(self):
        return {
            'author':self.author,
            'content':self.content,
            'email':self.email,
            'id':self.pk
        }

class CommentGames2(models.Model):
    author = models.CharField(max_length=100, default='DEFAULT VALUE')
    content = models.TextField()
    email = models.EmailField(default='DEFAULT VALUE')

    def __str__(self):
        return self.author
    
    def get_dict(self):
        return {
            'author':self.author,
            'content':self.content,
            'email':self.email,
            'id':self.pk
        }



