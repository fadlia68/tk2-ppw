from django.contrib import admin
from .models import Comment, Article

admin.register(Comment, Article)(admin.ModelAdmin)
