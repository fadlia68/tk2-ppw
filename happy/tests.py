from django.test import TestCase
from django.apps import apps
from django.test import Client
from django.urls import resolve, reverse
from .apps import HappyConfig
from .forms import CommentForm, SearchForm
from .models import Article, Comment, CommentKdrama, CommentAnime, CommentRomantic, CommentPodcast, CommentKpop, CommentTiktok, CommentGames1, CommentGames2
from .forms import CommentForm, SearchForm
from .views import watch, games, music,searchKeyword, detail, anime, deleteAnime, games1, deleteGames1, games2, deleteGames2, kdrama, deleteKdrama, kpop, deleteKpop, netflix, deleteNetflix, podcast, deletePodcast, romantic, deleteRomantic, tiktok, deleteTiktok 


class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(HappyConfig.name, 'happy')
        self.assertEqual(apps.get_app_config('happy').name, 'happy')

class ModelTest(TestCase):
    def setUp(self):
        self.article = Article.objects.create(
            name="netflix",
            title="Top 10 Netflix series to watch in 2020",
            foto="netflix.png",
            keyword="netflix series watching horror romance drama fantasy thriller tv show watch"
            )
        self.comment = Comment.objects.create(
            author="Ayuka",
            content="amazing!",
            email="ayukasnht@gmai.com"
            )
        self.commentkdrama = CommentKdrama.objects.create(
            author="Ayuka",
            content="amazing!",
            email="ayukasnht@gmai.com"
            )
        self.commentanime = CommentAnime.objects.create(
            author="Ayuka",
            content="amazing!",
            email="ayukasnht@gmai.com"
            )
        self.commentromantic = CommentRomantic.objects.create(
            author="Ayuka",
            content="amazing!",
            email="ayukasnht@gmai.com"
            )
        self.commentkpop = CommentKpop.objects.create(
            author="Ayuka",
            content="amazing!",
            email="ayukasnht@gmai.com"
            )
        self.commentpodcast = CommentPodcast.objects.create(
            author="Ayuka",
            content="amazing!",
            email="ayukasnht@gmai.com"
            )
        self.commenttiktok = CommentTiktok.objects.create(
            author="Ayuka",
            content="amazing!",
            email="ayukasnht@gmai.com"
            )
        self.commentgames1 = CommentGames1.objects.create(
            author="Ayuka",
            content="amazing!",
            email="ayukasnht@gmai.com"
            )
        self.commentgames2 = CommentGames2.objects.create(
            author="Ayuka",
            content="amazing!",
            email="ayukasnht@gmai.com"
            )
        

    def test_instance_created(self):
        self.assertEqual(Article.objects.count(), 1)
        self.assertEqual(Comment.objects.count(), 1)
        self.assertEqual(CommentKdrama.objects.count(), 1)
        self.assertEqual(CommentRomantic.objects.count(), 1)
        self.assertEqual(CommentAnime.objects.count(), 1)
        self.assertEqual(CommentGames1.objects.count(), 1)
        self.assertEqual(CommentGames2.objects.count(), 1)
        self.assertEqual(CommentPodcast.objects.count(), 1)
        self.assertEqual(CommentTiktok.objects.count(), 1)
        self.assertEqual(CommentKpop.objects.count(), 1)


    def test_str(self):
        self.assertEqual(str(self.article), "netflix")
        self.assertEqual(str(self.comment), "Ayuka")
        self.assertEqual(str(self.commentanime), "Ayuka")
        self.assertEqual(str(self.commentkdrama), "Ayuka")
        self.assertEqual(str(self.commentkpop), "Ayuka")
        self.assertEqual(str(self.commenttiktok), "Ayuka")
        self.assertEqual(str(self.commentromantic), "Ayuka")
        self.assertEqual(str(self.commentpodcast), "Ayuka")
        self.assertEqual(str(self.commentgames1), "Ayuka")
        self.assertEqual(str(self.commentgames2), "Ayuka")

class FormTest(TestCase):
    def test_comment_form_is_valid(self):
        comment_form = CommentForm(data={
            "author": "Ayuka",
            "email": "ayukasnht@gmail.com",
            "content": "amazing!",
        })
        self.assertTrue(comment_form.is_valid())

    def test_comment_form_invalid(self):
        comment_form = CommentForm(data={})
        self.assertFalse(comment_form.is_valid())
    
    def test_search_form_is_valid(self):
        search_form = SearchForm(data={
            "input": "series",
        })
        self.assertTrue(search_form.is_valid())

    def test_search_form_invalid(self):
        search_form = SearchForm(data={})
        self.assertFalse(search_form.is_valid())

class UrlsTest(TestCase):
    def setUp(self):
        self.article = Article.objects.create(
            name="netflix",
            title="Top 10 Netflix series to watch in 2020",
            foto="netflix.png",
            keyword="netflix series watching horror romance drama fantasy thriller tv show watch"
            )
        self.comment = Comment.objects.create(
            author="Ayuka",
            content="amazing!",
            email="ayukasnht@gmai.com"
            )
        self.commentkdrama = CommentKdrama.objects.create(
            author="Ayuka",
            content="amazing!",
            email="ayukasnht@gmai.com"
            )
        self.commentanime = CommentAnime.objects.create(
            author="Ayuka",
            content="amazing!",
            email="ayukasnht@gmai.com"
            )
        self.commentromantic = CommentRomantic.objects.create(
            author="Ayuka",
            content="amazing!",
            email="ayukasnht@gmai.com"
            )
        self.commentkpop = CommentKpop.objects.create(
            author="Ayuka",
            content="amazing!",
            email="ayukasnht@gmai.com"
            )
        self.commentpodcast = CommentPodcast.objects.create(
            author="Ayuka",
            content="amazing!",
            email="ayukasnht@gmai.com"
            )
        self.commenttiktok = CommentTiktok.objects.create(
            author="Ayuka",
            content="amazing!",
            email="ayukasnht@gmai.com"
            )
        self.commentgames1 = CommentGames1.objects.create(
            author="Ayuka",
            content="amazing!",
            email="ayukasnht@gmai.com"
            )
        self.commentgames2 = CommentGames2.objects.create(
            author="Ayuka",
            content="amazing!",
            email="ayukasnht@gmai.com"
            )

        self.watch = reverse("happy:watch")
        self.games = reverse("happy:games")
        self.music = reverse("happy:music")
        self.search = reverse("happy:searchKeyword")
        self.detail = reverse("happy:detail", args=[self.article.name])

        self.netflix = reverse("happy:netflix")
        self.deletenetflix = reverse("happy:deleteNetflix", args=[self.comment.pk])
        
        self.anime = reverse("happy:anime")
        self.deleteanime = reverse("happy:deleteAnime", args=[self.commentanime.pk])
        
        self.kdrama = reverse("happy:kdrama")
        self.deletekdrama = reverse("happy:deleteKdrama", args=[self.commentkdrama.pk])
       
        self.romantic = reverse("happy:romantic")
        self.deleteromantic = reverse("happy:deleteRomantic", args=[self.commentromantic.pk])
        
        self.kpop = reverse("happy:kpop")
        self.deletekpop = reverse("happy:deleteKpop", args=[self.commentkpop.pk])
        
        self.tiktok = reverse("happy:tiktok")
        self.deletetiktok = reverse("happy:deleteTiktok", args=[self.commenttiktok.pk])
        
        self.podcast = reverse("happy:podcast")
        self.deletepodcast = reverse("happy:deletePodcast", args=[self.commentpodcast.pk])
        
        self.games1 = reverse("happy:games1")
        self.deletegames1 = reverse("happy:deleteGames1", args=[self.commentgames1.pk])
        
        self.games2 = reverse("happy:games2")
        self.deletegames2 = reverse("happy:deleteGames2", args=[self.commentgames2.pk])
    
    def test_watch_use_right_function(self):
        found = resolve(self.watch)
        self.assertEqual(found.func, watch)
    
    def test_games_use_right_function(self):
        found = resolve(self.games)
        self.assertEqual(found.func, games)
    
    def test_music_use_right_function(self):
        found = resolve(self.music)
        self.assertEqual(found.func, music)
    
    def test_search_use_right_function(self):
        found = resolve(self.search)
        self.assertEqual(found.func, searchKeyword)
    
    def test_detail_use_right_function(self):
        found = resolve(self.detail)
        self.assertEqual(found.func, detail)
    
    def test_netflix_use_right_function(self):
        found = resolve(self.netflix)
        self.assertEqual(found.func, netflix)
    
    def test_deletenetflix_use_right_function(self):
        found = resolve(self.deletenetflix)
        self.assertEqual(found.func, deleteNetflix)
    
    def test_anime_use_right_function(self):
        found = resolve(self.anime)
        self.assertEqual(found.func, anime)
    
    def test_deleteanime_use_right_function(self):
        found = resolve(self.deleteanime)
        self.assertEqual(found.func, deleteAnime)
    
    def test_kdrama_use_right_function(self):
        found = resolve(self.kdrama)
        self.assertEqual(found.func, kdrama)
    
    def test_deletekdrama_use_right_function(self):
        found = resolve(self.deletekdrama)
        self.assertEqual(found.func, deleteKdrama)
    
    def test_romantic_use_right_function(self):
        found = resolve(self.romantic)
        self.assertEqual(found.func, romantic)
    
    def test_deleteromantic_use_right_function(self):
        found = resolve(self.deleteromantic)
        self.assertEqual(found.func, deleteRomantic)
    
    def test_kpop_use_right_function(self):
        found = resolve(self.kpop)
        self.assertEqual(found.func, kpop)
    
    def test_deletekpop_use_right_function(self):
        found = resolve(self.deletekpop)
        self.assertEqual(found.func, deleteKpop)
    
    def test_podcast_use_right_function(self):
        found = resolve(self.podcast)
        self.assertEqual(found.func, podcast)
    
    def test_deletepodcast_use_right_function(self):
        found = resolve(self.deletepodcast)
        self.assertEqual(found.func, deletePodcast)
    
    def test_tiktok_use_right_function(self):
        found = resolve(self.tiktok)
        self.assertEqual(found.func, tiktok)
    
    def test_deletetiktok_use_right_function(self):
        found = resolve(self.deletetiktok)
        self.assertEqual(found.func, deleteTiktok)
    
    def test_games1_use_right_function(self):
        found = resolve(self.games1)
        self.assertEqual(found.func, games1)
    
    def test_deletegames1_use_right_function(self):
        found = resolve(self.deletegames1)
        self.assertEqual(found.func, deleteGames1)
    
    def test_games2_use_right_function(self):
        found = resolve(self.games2)
        self.assertEqual(found.func, games2)
    
    def test_deletegames2_use_right_function(self):
        found = resolve(self.deletegames2)
        self.assertEqual(found.func, deleteGames2)

class ViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.home = reverse("happy:home")
        self.watch = reverse("happy:watch")
        self.games = reverse("happy:games")
        self.music = reverse("happy:music")
        self.netflix = reverse("happy:netflix")
        self.anime = reverse("happy:anime")
        self.kdrama = reverse("happy:kdrama")
        self.romantic = reverse("happy:romantic")
        self.kpop = reverse("happy:kpop")
        self.tiktok = reverse("happy:tiktok")
        self.podcast = reverse("happy:podcast")
        self.games1 = reverse("happy:games1")
        self.games2 = reverse("happy:games2")

    def test_GET_home(self):
        response = self.client.get(self.home)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'home.html')

    def test_GET_watch(self):
        response = self.client.get(self.watch)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'watch.html')
    
    def test_GET_music(self):
        response = self.client.get(self.music)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'music.html')
    
    def test_GET_games(self):
        response = self.client.get(self.games)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'games.html')
    
    def test_GET_search(self):
        article =  Article(
            name="netflix",
            title="Top 10 Netflix series to watch in 2020",
            foto="netflix.png",
            keyword="netflix series watching horror romance drama fantasy thriller tv show watch"
        )
        article.save()
        response = self.client.get('/happy/search/?search_keyword=series')
        self.assertEqual(response.status_code, 200)
    
    def test_GET_detail_netflix(self):
        article =  Article(
            name="netflix",
            title="Top 10 Netflix series to watch in 2020",
            foto="netflix.png",
            keyword="netflix series watching horror romance drama fantasy thriller tv show watch"
        )
        article.save()
        response = self.client.get(reverse('happy:detail', args=[article.name]))
        self.assertEqual(response.status_code, 302)
    
    def test_GET_detail_romantic(self):
        article =  Article(
            name="romantic",
            title="Top 10 Netflix series to watch in 2020",
            foto="netflix.png",
            keyword="netflix series watching horror romance drama fantasy thriller tv show watch"
        )
        article.save()
        response = self.client.get(reverse('happy:detail', args=[article.name]))
        self.assertEqual(response.status_code, 302)
    
    def test_GET_detail_kdrama(self):
        article =  Article(
            name="kdrama",
            title="Top 10 Netflix series to watch in 2020",
            foto="netflix.png",
            keyword="netflix series watching horror romance drama fantasy thriller tv show watch"
        )
        article.save()
        response = self.client.get(reverse('happy:detail', args=[article.name]))
        self.assertEqual(response.status_code, 302)
    
    def test_GET_detail_anime(self):
        article =  Article(
            name="anime",
            title="Top 10 Netflix series to watch in 2020",
            foto="netflix.png",
            keyword="netflix series watching horror romance drama fantasy thriller tv show watch"
        )
        article.save()
        response = self.client.get(reverse('happy:detail', args=[article.name]))
        self.assertEqual(response.status_code, 302)
    
    def test_GET_detail_tiktok(self):
        article =  Article(
            name="tiktok",
            title="Top 10 Netflix series to watch in 2020",
            foto="netflix.png",
            keyword="netflix series watching horror romance drama fantasy thriller tv show watch"
        )
        article.save()
        response = self.client.get(reverse('happy:detail', args=[article.name]))
        self.assertEqual(response.status_code, 302)
    
    def test_GET_detail_kpop(self):
        article =  Article(
            name="kpop",
            title="Top 10 Netflix series to watch in 2020",
            foto="netflix.png",
            keyword="netflix series watching horror romance drama fantasy thriller tv show watch"
        )
        article.save()
        response = self.client.get(reverse('happy:detail', args=[article.name]))
        self.assertEqual(response.status_code, 302)
    
    def test_GET_detail_podcast(self):
        article =  Article(
            name="podcast",
            title="Top 10 Netflix series to watch in 2020",
            foto="netflix.png",
            keyword="netflix series watching horror romance drama fantasy thriller tv show watch"
        )
        article.save()
        response = self.client.get(reverse('happy:detail', args=[article.name]))
        self.assertEqual(response.status_code, 302)
    
    def test_GET_detail_games1(self):
        article =  Article(
            name="games1",
            title="Top 10 Netflix series to watch in 2020",
            foto="netflix.png",
            keyword="netflix series watching horror romance drama fantasy thriller tv show watch"
        )
        article.save()
        response = self.client.get(reverse('happy:detail', args=[article.name]))
        self.assertEqual(response.status_code, 302)
    
    def test_GET_detail_games2(self):
        article =  Article(
            name="games2",
            title="Top 10 Netflix series to watch in 2020",
            foto="netflix.png",
            keyword="netflix series watching horror romance drama fantasy thriller tv show watch"
        )
        article.save()
        response = self.client.get(reverse('happy:detail', args=[article.name]))
        self.assertEqual(response.status_code, 302)
    
    def test_GET_netflix(self):
        response = self.client.get(self.netflix)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'netflix.html')

    def test_GET_kdrama(self):
        response = self.client.get(self.kdrama)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'kdrama.html')
    
    def test_GET_romantic(self):
        response = self.client.get(self.romantic)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'romantic.html')
    
    def test_GET_anime(self):
        response = self.client.get(self.anime)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'anime.html')

    def test_GET_tiktok(self):
        response = self.client.get(self.tiktok)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'tiktok.html')
    
    def test_GET_kpop(self):
        response = self.client.get(self.kpop)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'kpop.html')
    
    def test_GET_podcast(self):
        response = self.client.get(self.podcast)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'podcast.html')

    def test_GET_games1(self):
        response = self.client.get(self.games1)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'games1.html')
    
    def test_GET_games2(self):
        response = self.client.get(self.games2)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'games2.html')

    def test_POST_netflix(self):
        response = self.client.post(self.netflix,
        {
            'author': 'Ayuka',
            'email': 'ayukasnht@gmail.com',
            'content': 'amazing!',
        }, follow=True)
        self.assertEqual(response.status_code, 200)
    
    def test_POST_netflix_invalid(self):
        response = self.client.post(self.netflix,
        {
            'author': '',
            'email': '',
            'content': '',
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {'success': False}
        )
    
    def test_GET_deleteNetflix(self):
        comment =  Comment(
            author="Ayuka",
            content="amazing!",
            email="ayukasnht@gmai.com"
        )
        comment.save()
        response = self.client.get(reverse('happy:deleteNetflix', args=[comment.pk]))
        self.assertEqual(Comment.objects.count(), 0)
        self.assertEqual(response.status_code, 200)
    
    def test_GET_deleteNetflix_Failed(self):
        comment =  Comment(
            author="Ayuka",
            content="amazing!",
            email="ayukasnht@gmai.com"
        )
        comment.save()
        response = self.client.get(reverse('happy:deleteNetflix', args=["100"]))
        self.assertEqual(Comment.objects.count(), 1)
        self.assertEqual(response.status_code, 200)
    
    def test_GET_commentNetflix(self):
        response = self.client.get(reverse('happy:commentNetflix'))
        self.assertEqual(response.status_code, 200)
    
    def test_POST_anime(self):
        response = self.client.post(self.anime,
        {
            'author': 'Ayuka',
            'email': 'ayukasnht@gmail.com',
            'content': 'amazing!',
        }, follow=True)
        self.assertEqual(response.status_code, 200)
    
    def test_POST_anime_invalid(self):
        response = self.client.post(self.anime,
        {
            'author': '',
            'email': '',
            'content': '',
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {'success': False}
        )
    
    def test_GET_deleteAnime(self):
        comment =  CommentAnime(
            author="Ayuka",
            content="amazing!",
            email="ayukasnht@gmai.com"
        )
        comment.save()
        response = self.client.get(reverse('happy:deleteAnime', args=[comment.pk]))
        self.assertEqual(CommentAnime.objects.count(), 0)
        self.assertEqual(response.status_code, 200)
    
    def test_GET_deleteAnime_Failed(self):
        comment =  Comment(
            author="Ayuka",
            content="amazing!",
            email="ayukasnht@gmai.com"
        )
        comment.save()
        response = self.client.get(reverse('happy:deleteAnime', args=["100"]))
        self.assertEqual(Comment.objects.count(), 1)
        self.assertEqual(response.status_code, 200)

    def test_GET_commentAnime(self):
        response = self.client.get(reverse('happy:commentAnime'))
        self.assertEqual(response.status_code, 200)
    
    def test_POST_kdrama(self):
        response = self.client.post(self.kdrama,
        {
            'author': 'Ayuka',
            'email': 'ayukasnht@gmail.com',
            'content': 'amazing!',
        }, follow=True)
        self.assertEqual(response.status_code, 200)
    
    def test_POST_kdrama_invalid(self):
        response = self.client.post(self.kdrama,
        {
            'author': '',
            'email': '',
            'content': '',
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {'success': False}
        )
    
    def test_GET_deleteKdrama(self):
        comment =  CommentKdrama(
            author="Ayuka",
            content="amazing!",
            email="ayukasnht@gmai.com"
        )
        comment.save()
        response = self.client.get(reverse('happy:deleteKdrama', args=[comment.pk]))
        self.assertEqual(CommentKdrama.objects.count(), 0)
        self.assertEqual(response.status_code, 200)
    
    def test_GET_deleteKdrama_Failed(self):
        comment =  Comment(
            author="Ayuka",
            content="amazing!",
            email="ayukasnht@gmai.com"
        )
        comment.save()
        response = self.client.get(reverse('happy:deleteKdrama', args=["100"]))
        self.assertEqual(Comment.objects.count(), 1)
        self.assertEqual(response.status_code, 200)
    
    def test_GET_commentKdrama(self):
        response = self.client.get(reverse('happy:commentKdrama'))
        self.assertEqual(response.status_code, 200)
    
    def test_POST_romantic(self):
        response = self.client.post(self.romantic,
        {
            'author': 'Ayuka',
            'email': 'ayukasnht@gmail.com',
            'content': 'amazing!',
        }, follow=True)
        self.assertEqual(response.status_code, 200)
    
    def test_POST_romantic_invalid(self):
        response = self.client.post(self.romantic,
        {
            'author': '',
            'email': '',
            'content': '',
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {'success': False}
        )
    
    def test_GET_deleteRomantic(self):
        comment =  CommentRomantic(
            author="Ayuka",
            content="amazing!",
            email="ayukasnht@gmai.com"
        )
        comment.save()
        response = self.client.get(reverse('happy:deleteRomantic', args=[comment.pk]))
        self.assertEqual(CommentRomantic.objects.count(), 0)
        self.assertEqual(response.status_code, 200)
    
    def test_GET_deleteRomantic_Failed(self):
        comment =  Comment(
            author="Ayuka",
            content="amazing!",
            email="ayukasnht@gmai.com"
        )
        comment.save()
        response = self.client.get(reverse('happy:deleteRomantic', args=["100"]))
        self.assertEqual(Comment.objects.count(), 1)
        self.assertEqual(response.status_code, 200)
    
    def test_GET_commentRomantic(self):
        response = self.client.get(reverse('happy:commentRomantic'))
        self.assertEqual(response.status_code, 200)
    
    def test_POST_podcast(self):
        response = self.client.post(self.podcast,
        {
            'author': 'Ayuka',
            'email': 'ayukasnht@gmail.com',
            'content': 'amazing!',
        }, follow=True)
        self.assertEqual(response.status_code, 200)
    
    def test_POST_podcast_invalid(self):
        response = self.client.post(self.podcast,
        {
            'author': '',
            'email': '',
            'content': '',
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {'success': False}
        )
    
    def test_GET_deletePodcast(self):
        comment =  CommentPodcast(
            author="Ayuka",
            content="amazing!",
            email="ayukasnht@gmai.com"
        )
        comment.save()
        response = self.client.get(reverse('happy:deletePodcast', args=[comment.pk]))
        self.assertEqual(CommentPodcast.objects.count(), 0)
        self.assertEqual(response.status_code, 200)
    
    def test_GET_deletePodcast_Failed(self):
        comment =  Comment(
            author="Ayuka",
            content="amazing!",
            email="ayukasnht@gmai.com"
        )
        comment.save()
        response = self.client.get(reverse('happy:deletePodcast', args=["100"]))
        self.assertEqual(Comment.objects.count(), 1)
        self.assertEqual(response.status_code, 200)
    
    def test_GET_commentPodcast(self):
        response = self.client.get(reverse('happy:commentPodcast'))
        self.assertEqual(response.status_code, 200)
    
    def test_POST_tiktok(self):
        response = self.client.post(self.tiktok,
        {
            'author': 'Ayuka',
            'email': 'ayukasnht@gmail.com',
            'content': 'amazing!',
        }, follow=True)
        self.assertEqual(response.status_code, 200)
    
    def test_POST_tiktok_invalid(self):
        response = self.client.post(self.tiktok,
        {
            'author': '',
            'email': '',
            'content': '',
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {'success': False}
        )
    
    def test_GET_deleteTiktok(self):
        comment =  CommentTiktok(
            author="Ayuka",
            content="amazing!",
            email="ayukasnht@gmai.com"
        )
        comment.save()
        response = self.client.get(reverse('happy:deleteTiktok', args=[comment.pk]))
        self.assertEqual(CommentTiktok.objects.count(), 0)
        self.assertEqual(response.status_code, 200)
    
    def test_GET_deleteTiktok_Failed(self):
        comment =  Comment(
            author="Ayuka",
            content="amazing!",
            email="ayukasnht@gmai.com"
        )
        comment.save()
        response = self.client.get(reverse('happy:deleteTiktok', args=["100"]))
        self.assertEqual(Comment.objects.count(), 1)
        self.assertEqual(response.status_code, 200)
    
    def test_GET_commentTiktok(self):
        response = self.client.get(reverse('happy:commentTiktok'))
        self.assertEqual(response.status_code, 200)
    
    def test_POST_kpop(self):
        response = self.client.post(self.kpop,
        {
            'author': 'Ayuka',
            'email': 'ayukasnht@gmail.com',
            'content': 'amazing!',
        }, follow=True)
        self.assertEqual(response.status_code, 200)
    
    def test_POST_kpop_invalid(self):
        response = self.client.post(self.kpop,
        {
            'author': '',
            'email': '',
            'content': '',
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {'success': False}
        )
    
    def test_GET_deleteKpop(self):
        comment =  CommentKpop(
            author="Ayuka",
            content="amazing!",
            email="ayukasnht@gmai.com"
        )
        comment.save()
        response = self.client.get(reverse('happy:deleteKpop', args=[comment.pk]))
        self.assertEqual(CommentKpop.objects.count(), 0)
        self.assertEqual(response.status_code, 200)
    
    def test_GET_deleteKpop_Failed(self):
        comment =  Comment(
            author="Ayuka",
            content="amazing!",
            email="ayukasnht@gmai.com"
        )
        comment.save()
        response = self.client.get(reverse('happy:deleteKpop', args=["100"]))
        self.assertEqual(Comment.objects.count(), 1)
        self.assertEqual(response.status_code, 200)
    
    def test_GET_commentKpop(self):
        response = self.client.get(reverse('happy:commentKpop'))
        self.assertEqual(response.status_code, 200)
    
    def test_POST_games1(self):
        response = self.client.post(self.games1,
        {
            'author': 'Ayuka',
            'email': 'ayukasnht@gmail.com',
            'content': 'amazing!',
        }, follow=True)
        self.assertEqual(response.status_code, 200)
    
    def test_POST_games1_invalid(self):
        response = self.client.post(self.games1,
        {
            'author': '',
            'email': '',
            'content': '',
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {'success': False}
        )
    
    def test_GET_deleteGames1(self):
        comment =  CommentGames1(
            author="Ayuka",
            content="amazing!",
            email="ayukasnht@gmai.com"
        )
        comment.save()
        response = self.client.get(reverse('happy:deleteGames1', args=[comment.pk]))
        self.assertEqual(CommentGames1.objects.count(), 0)
        self.assertEqual(response.status_code, 200)
    
    def test_GET_deleteGames1_Failed(self):
        comment =  Comment(
            author="Ayuka",
            content="amazing!",
            email="ayukasnht@gmai.com"
        )
        comment.save()
        response = self.client.get(reverse('happy:deleteGames1', args=["100"]))
        self.assertEqual(Comment.objects.count(), 1)
        self.assertEqual(response.status_code, 200)
    
    def test_GET_commentGames1(self):
        response = self.client.get(reverse('happy:commentGames1'))
        self.assertEqual(response.status_code, 200)
    
    def test_POST_games2(self):
        response = self.client.post(self.games2,
        {
            'author': 'Ayuka',
            'email': 'ayukasnht@gmail.com',
            'content': 'amazing!',
        }, follow=True)
        self.assertEqual(response.status_code, 200)
    
    def test_POST_games2_invalid(self):
        response = self.client.post(self.games2,
        {
            'author': '',
            'email': '',
            'content': '',
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {'success': False}
        )
    
    def test_GET_deleteGames2(self):
        comment =  CommentGames2(
            author="Ayuka",
            content="amazing!",
            email="ayukasnht@gmai.com"
        )
        comment.save()
        response = self.client.get(reverse('happy:deleteGames2', args=[comment.pk]))
        self.assertEqual(Comment.objects.count(), 0)
        self.assertEqual(response.status_code, 200)
    
    def test_GET_deleteGames2_Failed(self):
        comment =  Comment(
            author="Ayuka",
            content="amazing!",
            email="ayukasnht@gmai.com"
        )
        comment.save()
        response = self.client.get(reverse('happy:deleteGames2', args=["100"]))
        self.assertEqual(Comment.objects.count(), 1)
        self.assertEqual(response.status_code, 200)
    
    def test_GET_commentGames2(self):
        response = self.client.get(reverse('happy:commentGames2'))
        self.assertEqual(response.status_code, 200)