$(document).ready(function(){
    showCommentGames2()
  
    $("#createComment-Games2").click(function(e){
      e.preventDefault();
      submitFormGames2();
      $('#commentForm input[type="text"]').val('');
      $('#commentForm textarea').val('');
    })
  
    $("#delete-comment-games2").click(function(e){
      e.preventDefault();
      deleteCommentGames2(e);
    })
  })
  
  function showCommentGames2() {
    var resultK = '';
  
      $.ajax({
        url: "/happy/commentGames2/",
        error: function(){
          alert("Something went wrong, try again.")
        },
  
        success: function (response) {
          console.log(response);
          var comment_img = $('body').data('bubble-img');
  
          $('#commentSection-Games2').empty();
  
          for (var i = 0; i < response.data.length; i++) {
            comment = response.data[i];
  
            resultK += 
              `<div class="comment-list">
              <div class="single-comment justify-content-between d-flex">
                <div class="user justify-content-between d-flex">
                    <div class="thumb">
                      <img src="${comment_img}" alt="">
                    </div>
                    <div class="desc">
                      <p id="p-comment" class="comment">
                        ${comment.content}
                      </p>
                      <div class="d-flex justify-content-between">
                          <div class="d-flex align-items-center">
                            <h5>
                              <p style="color:rgb(110, 38, 128); font-size:17px; font-family:'Josefin Sans';">${comment.author}</p>
                            </h5>
                            </div>
                            </div>
                            <button style="padding: 6px 3px; background-color: white; color:black; font-size:15px;" id="delete-comment-games2" onClick="deleteCommentGames2(this)" data-id="${comment.id}" class="btn">
                              <div class="timeline-badge"><i class="fa fa-trash"></i></div>
                            </button>
                    </div>
                </div>
              </div>
              </div>` 
          }
          console.log(comment)
          $('#commentSection-Games2').append(resultK);
        }
      })
  }
  
  
  function submitFormGames2() {
    $.ajax({
      url: "/happy/games2/",
      type: 'POST',
      data: $("form#commentForm").serialize(),
      error: function(){
        alert("Something went wrong, try again.")
      },
  
      success: function (data) {
        if (data.success === true) {
          showCommentGames2();
          alert("Success!")
        } else if (data.success === false) {
          alert("Failed")
        }
      }
    })
  }
  
  function deleteCommentGames2(elem) {
    var comment_id = $(elem).data('id')
    console.log(comment_id)
    $.ajax({
      url: `/happy/deleteGames2/${comment_id}`,
      type: 'POST',
      data: $("form#commentForm").serialize(),
      error: function(){
        alert("Something went wrong, try again.")
      },
  
      success: function (data) {
        if (data.success === true) {
          showCommentGames2();
          alert("Deleted!")
        } else if (data.success === false) {
          alert("Failed!")
        }
      }
    })
  }
  
  
  
  
  
  
  
  