// HAPPY

$(document).ready(function(){
    showComment()
  
    $("#createComment").click(function(e){
      e.preventDefault();
      submitForm();
      $('#commentForm input[type="text"]').val('');
      $('#commentForm textarea').val('');
    })
  
    $("#delete-comment").click(function(e){
      e.preventDefault();
      deleteComment(e);
    })
  })
  
  
  function showComment() {
    var result = '';
  
      $.ajax({
        url: "/happy/commentNetflix/",
        error: function(){
          alert("Something went wrong, try again.")
        },
  
        success: function (response) {
          console.log(response);
          var comment_img = $('body').data('bubble-img');
  
          $('#commentSection').empty();
  
          for (var i = 0; i < response.data.length; i++) {
            comment = response.data[i];
  
            result += 
              `<div class="comment-list">
              <div class="single-comment justify-content-between d-flex">
                <div class="user justify-content-between d-flex">
                    <div class="thumb">
                      <img src="${comment_img}" alt="">
                    </div>
                    <div class="desc">
                      <p id="p-comment" class="comment">
                        ${comment.content}
                      </p>
                      <div class="d-flex justify-content-between">
                          <div class="d-flex align-items-center">
                            <h5>
                              <p style="color:rgb(110, 38, 128); font-size:17px; font-family:'Josefin Sans';">${comment.author}</p>
                            </h5>
                            </div>
                            </div>
                            <button style="padding: 6px 3px; background-color: white; color:black; font-size:15px;" id="delete-comment" onClick="deleteComment(this)" data-id="${comment.id}" class="btn">
                              <div class="timeline-badge"><i class="fa fa-trash"></i></div>
                            </button>
                    </div>
                </div>
              </div>
              </div>` 
          }
          console.log(comment)
          $('#commentSection').append(result);
        }
      })
  }
  
  
  function submitForm() {
    $.ajax({
      url: "/happy/netflix/",
      type: 'POST',
      data: $("form#commentForm").serialize(),
      error: function(){
        alert("Something went wrong, try again.")
      },
  
      success: function (data) {
        if (data.success === true) {
          showComment();
          alert("Success!")
        } else if (data.success === false) {
          alert("Failed")
        }
      }
    })
  }
  
  function deleteComment(elem) {
    var comment_id = $(elem).data('id')
    console.log(comment_id)
    $.ajax({
      url: `/happy/deleteNetflix/${comment_id}`,
      type: 'POST',
      data: $("form#commentForm").serialize(),
      error: function(){
        alert("Something went wrong, try again.")
      },
  
      success: function (data) {
        if (data.success === true) {
          showComment();
          alert("Deleted!")
        } else if (data.success === false) {
          alert("Failed!")
        }
      }
    })
  }
  
  
  
  
  
  
  
  