$(document).ready(function(){
    showCommentKpop()
  
    $("#createComment-Kpop").click(function(e){
      e.preventDefault();
      submitFormKpop();
      $('#commentForm input[type="text"]').val('');
      $('#commentForm textarea').val('');
    })
  
    $("#delete-comment-kpop").click(function(e){
      e.preventDefault();
      deleteCommentKpop(e);
    })
  })
  
  function showCommentKpop() {
    var resultK = '';
  
      $.ajax({
        url: "/happy/commentKpop/",
        error: function(){
          alert("Something went wrong, try again.")
        },
  
        success: function (response) {
          console.log(response);
          var comment_img = $('body').data('bubble-img');
  
          $('#commentSection-Kpop').empty();
  
          for (var i = 0; i < response.data.length; i++) {
            comment = response.data[i];
  
            resultK += 
              `<div class="comment-list">
              <div class="single-comment justify-content-between d-flex">
                <div class="user justify-content-between d-flex">
                    <div class="thumb">
                      <img src="${comment_img}" alt="">
                    </div>
                    <div class="desc">
                      <p id="p-comment" class="comment">
                        ${comment.content}
                      </p>
                      <div class="d-flex justify-content-between">
                          <div class="d-flex align-items-center">
                            <h5>
                              <p style="color:rgb(110, 38, 128); font-size:17px; font-family:'Josefin Sans';">${comment.author}</p>
                            </h5>
                            </div>
                            </div>
                            <button style="padding: 6px 3px; background-color: white; color:black; font-size:15px;" id="delete-comment-kpop" onClick="deleteCommentKpop(this)" data-id="${comment.id}" class="btn">
                              <div class="timeline-badge"><i class="fa fa-trash"></i></div>
                            </button>
                    </div>
                </div>
              </div>
              </div>` 
          }
          console.log(comment)
          $('#commentSection-Kpop').append(resultK);
        }
      })
  }
  
  
  function submitFormKpop() {
    $.ajax({
      url: "/happy/kpop/",
      type: 'POST',
      data: $("form#commentForm").serialize(),
      error: function(){
        alert("Something went wrong, try again.")
      },
  
      success: function (data) {
        if (data.success === true) {
          showCommentKpop();
          alert("Success!")
        } else if (data.success === false) {
          alert("Failed")
        }
      }
    })
  }
  
  function deleteCommentKpop(elem) {
    var comment_id = $(elem).data('id')
    console.log(comment_id)
    $.ajax({
      url: `/happy/deleteKpop/${comment_id}`,
      type: 'POST',
      data: $("form#commentForm").serialize(),
      error: function(){
        alert("Something went wrong, try again.")
      },
  
      success: function (data) {
        if (data.success === true) {
          showCommentKpop();
          alert("Deleted!")
        } else if (data.success === false) {
          alert("Failed!")
        }
      }
    })
  }
  
  
  
  
  
  
  
  