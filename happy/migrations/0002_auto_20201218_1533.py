# Generated by Django 3.1.4 on 2020-12-18 08:33

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('happy', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='comment',
            name='author',
        ),
        migrations.RemoveField(
            model_name='comment',
            name='email',
        ),
        migrations.RemoveField(
            model_name='commentanime',
            name='author',
        ),
        migrations.RemoveField(
            model_name='commentanime',
            name='email',
        ),
        migrations.RemoveField(
            model_name='commentgames1',
            name='author',
        ),
        migrations.RemoveField(
            model_name='commentgames1',
            name='email',
        ),
        migrations.RemoveField(
            model_name='commentgames2',
            name='author',
        ),
        migrations.RemoveField(
            model_name='commentgames2',
            name='email',
        ),
        migrations.RemoveField(
            model_name='commentkdrama',
            name='author',
        ),
        migrations.RemoveField(
            model_name='commentkdrama',
            name='email',
        ),
        migrations.RemoveField(
            model_name='commentkpop',
            name='author',
        ),
        migrations.RemoveField(
            model_name='commentkpop',
            name='email',
        ),
        migrations.RemoveField(
            model_name='commentpodcast',
            name='author',
        ),
        migrations.RemoveField(
            model_name='commentpodcast',
            name='email',
        ),
        migrations.RemoveField(
            model_name='commentromantic',
            name='author',
        ),
        migrations.RemoveField(
            model_name='commentromantic',
            name='email',
        ),
        migrations.RemoveField(
            model_name='commenttiktok',
            name='author',
        ),
        migrations.RemoveField(
            model_name='commenttiktok',
            name='email',
        ),
    ]
