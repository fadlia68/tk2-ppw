from django.urls import include, path
from . import views

app_name = 'happy'

urlpatterns = [
    path('', views.home, name='home'),
    path('watch/', views.watch, name='watch'),
    path('games/', views.games, name='games'),
    path('music/', views.music, name='music'),
    path('search/', views.searchKeyword, name='searchKeyword'),
    path('detail/<str:detail_id>/', views.detail, name='detail'),

    path('netflix/', views.netflix, name='netflix'),
    path('commentNetflix/', views.commentNetflix, name='commentNetflix'),
    path('deleteNetflix/<int:delete_id>', views.deleteNetflix, name='deleteNetflix'),

    path('anime/', views.anime, name='anime'),
    path('deleteAnime/<int:delete_id>', views.deleteAnime, name='deleteAnime'),
    path('commentAnime/', views.commentAnime, name='commentAnime'),

    path('kdrama/', views.kdrama, name='kdrama'),
    path('deleteKdrama/<int:delete_id>', views.deleteKdrama, name='deleteKdrama'),
    path('commentKdrama/', views.commentKdrama, name='commentKdrama'),

    path('romantic/', views.romantic, name='romantic'),
    path('deleteRomantic/<int:delete_id>', views.deleteRomantic, name='deleteRomantic'),
    path('commentRomantic/', views.commentRomantic, name='commentRomantic'),

    path('tiktok/', views.tiktok, name='tiktok'),
    path('deleteTiktok/<int:delete_id>', views.deleteTiktok, name='deleteTiktok'),
    path('commentTiktok/', views.commentTiktok, name='commentTiktok'),

    path('podcast/', views.podcast, name='podcast'),
    path('deletePodcast/<int:delete_id>', views.deletePodcast, name='deletePodcast'),
    path('commentPodcast/', views.commentPodcast, name='commentPodcast'),

    path('kpop/', views.kpop, name='kpop'),
    path('deleteKpop/<int:delete_id>', views.deleteKpop, name='deleteKpop'),
    path('commentKpop/', views.commentKpop, name='commentKpop'),

    path('games1/', views.games1, name='games1'),
    path('deleteGames1/<int:delete_id>', views.deleteGames1, name='deleteGames1'),
    path('commentGames1/', views.commentGames1, name='commentGames1'),

    path('games2/', views.games2, name='games2'),
    path('deleteGames2/<int:delete_id>', views.deleteGames2, name='deleteGames2'),
    path('commentGames2/', views.commentGames2, name='commentGames2'),

    # dilanjutkan ...
]
