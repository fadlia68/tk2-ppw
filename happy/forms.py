from django import forms 

class CommentForm(forms.Form):
    content = forms.CharField(
        label = "Write Comment",
        widget = forms.Textarea(
            attrs = {
                'class':'form-control w-100',
                'max_length : 100'
                'cols' : '30',
                'rows' : '10',
                'id' : 'content',
                'placeholder' : 'Write Comment',
            }
        )
    )

class SearchForm(forms.Form):
    input = forms.CharField(
        widget = forms.TextInput(
            attrs = {
                'class':'form-control',
                'max_length : 100'
                'id' : 'search',
                'placeholder' : 'Search Keyword',
            }
        )
    )