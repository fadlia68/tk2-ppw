from django.shortcuts import render, redirect 
from .models import Article, Comment, CommentKdrama, CommentPodcast, CommentAnime, CommentKpop, CommentRomantic, CommentGames1, CommentGames2, CommentTiktok
from . import forms
from django.http import JsonResponse
from django.contrib.auth.models import User

def home(request):
    return render(request, 'home.html')

def watch(request):
    return render(request, 'watch.html')

def music(request):
    return render(request, 'music.html')

def games(request):
    return render(request, 'games.html')

# =====================================================================================================

def searchKeyword(request):
    word = request.GET.get('search_keyword')
    object_list = Article.objects.filter(keyword__icontains=word)
    return render(request, 'searchView.html', {'list':object_list}) 

def detail(request, detail_id):
    blog_detail = Article.objects.get(name = detail_id)
    if blog_detail.name == 'netflix':
        return redirect('/happy/netflix')
    elif blog_detail.name == 'kdrama':
        return redirect('/happy/kdrama')
    elif blog_detail.name == 'romantic':
        return redirect('/happy/romantic')
    elif blog_detail.name == 'anime':
        return redirect('/happy/anime')
    elif blog_detail.name == 'tiktok':
        return redirect('/happy/tiktok')
    elif blog_detail.name == 'podcast':
        return redirect('/happy/podcast')
    elif blog_detail.name == 'kpop':
        return redirect('/happy/kpop')
    elif blog_detail.name == 'games1':
        return redirect('/happy/games1')
    elif blog_detail.name == 'games2':
        return redirect('/happy/games2')
    else:
        return redirect('')

# =====================================================================================================

def netflix(request):
    comments = {}
    if request.method ==  'POST':
        if request.is_ajax():
            comment_form = forms.CommentForm(request.POST)
            if comment_form.is_valid():
                data = comment_form.cleaned_data
                comment_input = Comment()
                comment_input.author = request.user.username
                comment_input.content = data['content']
                comment_input.save()
                return JsonResponse({'success': True})
        return JsonResponse({'success': False})
    else:
        comment_form = forms.CommentForm()
        data = Comment.objects.all()
        return render(request, 'netflix.html', {'form':comment_form, 'data':data})      
    
def commentNetflix(request):
    data = {
        'data':[i.get_dict() for i in Comment.objects.all()],
    }
    return JsonResponse(data=data)

def deleteNetflix(request, delete_id):
    try:
        deleted_subject = Comment.objects.get(pk = delete_id)
        deleted_subject.delete()
        return JsonResponse({'success': True})
    except:
        return JsonResponse({'success': False})  

# =====================================================================================================

def anime(request):
    comments = {}
    if request.method ==  'POST':
        if request.is_ajax():
            comment_form = forms.CommentForm(request.POST)
            if comment_form.is_valid():
                data = comment_form.cleaned_data
                comment_input = CommentAnime()
                comment_input.author = request.user.username
                comment_input.content = data['content']
                comment_input.save()
                return JsonResponse({'success': True})
        return JsonResponse({'success': False})
    else:
        comment_form = forms.CommentForm()
        data = CommentAnime.objects.all()
        return render(request, 'anime.html', {'form':comment_form, 'data':data})   

def commentAnime(request):
    data = {
        'data':[i.get_dict() for i in CommentAnime.objects.all()],
    }
    return JsonResponse(data=data)   

def deleteAnime(request, delete_id):
    try:
        deleted_subject = CommentAnime.objects.get(pk = delete_id)
        deleted_subject.delete()
        return JsonResponse({'success': True})
    except:
        return JsonResponse({'success': False}) 

# =====================================================================================================

def kdrama(request):
    comments = {}
    if request.method ==  'POST':
        if request.is_ajax():
            comment_form = forms.CommentForm(request.POST)
            if comment_form.is_valid():
                data = comment_form.cleaned_data
                comment_input = CommentKdrama()
                comment_input.author = request.user.username
                comment_input.content = data['content']
                comment_input.save()
                return JsonResponse({'success': True})
        return JsonResponse({'success': False})
    else:
        comment_form = forms.CommentForm()
        data = CommentKdrama.objects.all()
        return render(request, 'kdrama.html', {'form':comment_form, 'data':data})     

def commentKdrama(request):
    data = {
        'data':[i.get_dict() for i in CommentKdrama.objects.all()],
    }
    return JsonResponse(data=data)

def deleteKdrama(request, delete_id):
    try:
        deleted_subject = CommentKdrama.objects.get(pk = delete_id)
        deleted_subject.delete()
        return JsonResponse({'success': True})
    except:
        return JsonResponse({'success': False}) 

# =====================================================================================================

def podcast(request):
    comments = {}
    if request.method ==  'POST':
        if request.is_ajax():
            comment_form = forms.CommentForm(request.POST)
            if comment_form.is_valid():
                data = comment_form.cleaned_data
                comment_input = CommentPodcast()
                comment_input.author = request.user.username
                comment_input.content = data['content']
                comment_input.save()
                return JsonResponse({'success': True})
        return JsonResponse({'success': False})
    else:
        comment_form = forms.CommentForm()
        data = CommentPodcast.objects.all()
        return render(request, 'podcast.html', {'form':comment_form, 'data':data})     

def commentPodcast(request):
    data = {
        'data':[i.get_dict() for i in CommentPodcast.objects.all()],
    }
    return JsonResponse(data=data)

def deletePodcast(request, delete_id):
    try:
        deleted_subject = CommentPodcast.objects.get(pk = delete_id)
        deleted_subject.delete()
        return JsonResponse({'success': True})
    except:
        return JsonResponse({'success': False}) 

# =====================================================================================================

def kpop(request):
    comments = {}
    if request.method ==  'POST':
        if request.is_ajax():
            comment_form = forms.CommentForm(request.POST)
            if comment_form.is_valid():
                data = comment_form.cleaned_data
                comment_input = CommentKpop()
                comment_input.author = request.user.username
                comment_input.content = data['content']
                comment_input.save()
                return JsonResponse({'success': True})
        return JsonResponse({'success': False})
    else:
        comment_form = forms.CommentForm()
        data = CommentKpop.objects.all()
        return render(request, 'kpop.html', {'form':comment_form, 'data':data})     

def commentKpop(request):
    data = {
        'data':[i.get_dict() for i in CommentKpop.objects.all()],
    }
    return JsonResponse(data=data)

def deleteKpop(request, delete_id):
    try:
        deleted_subject = CommentKpop.objects.get(pk = delete_id)
        deleted_subject.delete()
        return JsonResponse({'success': True})
    except:
        return JsonResponse({'success': False}) 

# =====================================================================================================

def games1(request):
    comments = {}
    if request.method ==  'POST':
        if request.is_ajax():
            comment_form = forms.CommentForm(request.POST)
            if comment_form.is_valid():
                data = comment_form.cleaned_data
                comment_input = CommentGames1()
                comment_input.author = request.user.username
                comment_input.content = data['content']
                comment_input.save()
                return JsonResponse({'success': True})
        return JsonResponse({'success': False})
    else:
        comment_form = forms.CommentForm()
        data = CommentGames1.objects.all()
        return render(request, 'games1.html', {'form':comment_form, 'data':data})   

def commentGames1(request):
    data = {
        'data':[i.get_dict() for i in CommentGames1.objects.all()],
    }
    return JsonResponse(data=data)  

def deleteGames1(request, delete_id):
    try:
        deleted_subject = CommentGames1.objects.get(pk = delete_id)
        deleted_subject.delete()
        return JsonResponse({'success': True})
    except:
        return JsonResponse({'success': False})

# =====================================================================================================

def games2(request):
    comments = {}
    if request.method ==  'POST':
        if request.is_ajax():
            comment_form = forms.CommentForm(request.POST)
            if comment_form.is_valid():
                data = comment_form.cleaned_data
                comment_input = CommentGames2()
                comment_input.author = request.user.username
                comment_input.content = data['content']
                comment_input.save()
                return JsonResponse({'success': True})
        return JsonResponse({'success': False})
    else:
        comment_form = forms.CommentForm()
        data = CommentGames2.objects.all()
        return render(request, 'games2.html', {'form':comment_form, 'data':data})         
    
def commentGames2(request):
    data = {
        'data':[i.get_dict() for i in CommentGames2.objects.all()],
    }
    return JsonResponse(data=data)

def deleteGames2(request, delete_id):
    try:
        deleted_subject = CommentGames2.objects.get(pk = delete_id)
        deleted_subject.delete()
        return JsonResponse({'success': True})
    except:
        return JsonResponse({'success': False})

# =====================================================================================================

def romantic(request):
    comments = {}
    if request.method ==  'POST':
        if request.is_ajax():
            comment_form = forms.CommentForm(request.POST)
            if comment_form.is_valid():
                data = comment_form.cleaned_data
                comment_input = CommentRomantic()
                comment_input.author = request.user.username
                comment_input.content = data['content']
                comment_input.save()
                return JsonResponse({'success': True})
        return JsonResponse({'success': False})
    else:
        comment_form = forms.CommentForm()
        data = CommentRomantic.objects.all()
        return render(request, 'romantic.html', {'form':comment_form, 'data':data})          

def commentRomantic(request):
    data = {
        'data':[i.get_dict() for i in CommentRomantic.objects.all()],
    }
    return JsonResponse(data=data)

def deleteRomantic(request, delete_id):
    try:
        deleted_subject = CommentRomantic.objects.get(pk = delete_id)
        deleted_subject.delete()
        return JsonResponse({'success': True})
    except:
        return JsonResponse({'success': False}) 

# =====================================================================================================

def tiktok(request):
    comments = {}
    if request.method ==  'POST':
        if request.is_ajax():
            comment_form = forms.CommentForm(request.POST)
            if comment_form.is_valid():
                data = comment_form.cleaned_data
                comment_input = CommentTiktok()
                comment_input.author = request.user.username
                comment_input.content = data['content']
                comment_input.save()
                return JsonResponse({'success': True})
        return JsonResponse({'success': False})
    else:
        comment_form = forms.CommentForm()
        data = CommentTiktok.objects.all()
        return render(request, 'tiktok.html', {'form':comment_form, 'data':data})     

def commentTiktok(request):
    data = {
        'data':[i.get_dict() for i in CommentTiktok.objects.all()],
    }
    return JsonResponse(data=data)

def deleteTiktok(request, delete_id):
    try:
        deleted_subject = CommentTiktok.objects.get(pk = delete_id)
        deleted_subject.delete()
        return JsonResponse({'success': True})
    except:
        return JsonResponse({'success': False}) 

