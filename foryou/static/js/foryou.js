$(document).ready(function() {

    geser();
    function geser() {
        $.ajax({
            url: '/foryou/show',
            success: function(hasil) {
                var nama = hasil[0].fields.name;
                var awal = hasil[0].fields.awal;
                var pesan = hasil[0].fields.message;
                var like = hasil[0].fields.like;
                $("#nama").html('<span id="ava">' + awal + '</span>\n' + nama);
                $("#pesan").html(pesan);
                $(".myCount").html(like);
            }
        });
    }
    
    $(".prev").click(function() {
        geser();
    });

    $(".next").click(function() {
        geser();
    });

    $(".fa-heart-o").mousedown(function() {
        var nama = $("#nama").html();
        nama = nama.substring(24);
        var pesan = $("#pesan").html();
        $.ajax({
            url: '/foryou/like/?n=' + nama + '&m=' + pesan,
            success: function(hasil) {
                var nama = hasil[0].fields.name;
                var awal = hasil[0].fields.awal;
                var pesan = hasil[0].fields.message;
                var like = hasil[0].fields.like;
                $("#nama").html('<span id="ava">' + awal + '</span>\n' + nama);
                $("#pesan").html(pesan);
                $(".myCount").html(like);
            }
        });
        $(".fa-heart-o").css("display", "none");
        $(".fa-heart").css("display", "block");
    });

    $(".fa-heart").mouseup(function() {
        $(".fa-heart").css("display", "none");
        $(".fa-heart-o").css("display", "block")
    });


    $("#addButton").click(function() {
        $("#read2").slideToggle();
    });

    $("#buttonSend2").click(function() {
        var nama = $("input[name=name]").val();
        var pesan = $("textarea[name=message]").val();
        $.ajax({
            url: '/foryou/asave/?n=' + nama + '&m=' + pesan,
            success: function(hasil) {
                var nama = hasil[0].fields.name;
                var awal = hasil[0].fields.awal;
                var pesan = hasil[0].fields.message;
                var like = hasil[0].fields.like;
                $("#nama").html('<span id="ava">' + awal + '</span>\n' + nama);
                $("#pesan").html(pesan);
                $(".myCount").html(like);
            }
        });

    });

    $(".in_m2").keyup(function() {
        $("#limit").html("(" + $(".in_m2").val().length + "/200)");
    });

    $(".in_m").keyup(function() {
        $("#limit2").html("(" + $(".in_m").val().length + "/200)");
    })

});
