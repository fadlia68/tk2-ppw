from django.urls import include, path
from .views import index, read, send, save, history, show, asave, like

urlpatterns = [
    path('', index, name='index'),
    path('read/', read, name='read'),
    path('send/', send, name='send'),
    path('send/save', save, name='save'),
    path('history/', history, name='history'),
    path('show/', show, name='show'),
    path('asave/', asave, name='asave'),
    path('like/', like, name='like')
]