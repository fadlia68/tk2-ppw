from django.db import models
from django.contrib.auth.models import User

class Pesan(models.Model):
    name = models.CharField(max_length=50, default='Anonymous')
    awal = models.CharField(max_length=1, default='')
    message = models.TextField(max_length=200, default='')
    sender = models.CharField(max_length=150, default='')
    like = models.DecimalField(max_digits=10,decimal_places=0)
    date = models.DateField(null = True)