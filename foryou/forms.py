from django import forms
from .models import Pesan

class Input_Form(forms.ModelForm):
    class Meta:
        model = Pesan
        fields = ['name', 'message']
    input_name = {
        'type' : 'text',
        'placeholder' : 'leave it blank to be Anonymous',
        'class' : 'in_n'
    }
    input_message = {
        'type' : 'text',
        'placeholder' : 'write something',
        'class' : 'in_m'
    }
    name = forms.CharField(label='Name/Initial', required=False, widget=forms.TextInput(attrs=input_name))
    message = forms.CharField(label='Messages', max_length=200, required=True, widget=forms.Textarea(attrs=input_message))