from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
from django.contrib.auth.models import User
from django.core import serializers
from django.db.models import Max, Min
from .forms import Input_Form
from .models import Pesan
import random, datetime, json

lst = ["Alligator","Anteater","Armadillo","Auroch","Axolotl","Badger","Bat","Beaver","Buffalo","Camel","Capybara","Chameleon","Cheetah","Chinchilla","Chipmunk","Chupacabra","Cormorant","Coyote","Crow","Dingo","Dinosaur","Dolphin","Duck","Elephant","Ferret","Fox","Frog","Giraffe","Gopher","Grizzly","Hedgehog","Hippo","Hyena","Ibex","Ifrit","Iguana","Jackal","Kangaroo","Koala","Kraken","Lemur","Leopard","Liger","Llama","Manatee","Mink","Monkey","Moose","Narwhal","Orangutan","Otter","Panda","Penguin","Platypus","Pumpkin","Python","Quagga","Rabbit","Raccoon","Rhino","Sheep","Shrew","Skunk","Squirrel","Tiger","Turtle","Walrus","Wolf","Wolverine","Wombat"]
ada = False

def index(request):
    return render(request, 'main/index.html')

def read(request):
    message = Pesan.objects.all()
    response = {'messages' : message, 'success' : False}
    return render(request, 'main/read.html', response)

def send(request):
    response = {'form' : Input_Form}
    return render(request, 'main/send.html', response)

def save(request):
    form = Input_Form(request.POST)
    if (request.method == 'POST' and form.is_valid()):
        nama = form.cleaned_data['name']
        pesan = form.cleaned_data['message']
        user = ''
        if (request.user.is_authenticated):
            user = request.user.username
        if (nama == ''):
            hewan = random.choice(lst)
            lst.remove(hewan)
            new = Pesan(name="Anonymous " + hewan, message=pesan, awal=hewan[0], like=0, sender=user, date=datetime.date.today())
        else:
            new = Pesan(name=nama, message=pesan, awal=nama[0].upper(), like=0, sender=user, date=datetime.date.today())
        new.save()
        response = {'messages' : Pesan.objects.all(), 'success' : True}
        global ada
        ada = True
        return render(request, 'main/read.html', response)
    return HttpResponseRedirect('/foryou/send')

def history(request):
    response = {'messages' : Pesan.objects.filter(sender=request.user.username).order_by('-date')}
    return render(request, 'main/history.html', response)

def show(request):
    count = Pesan.objects.count()
    choose = random.randint(1, count)
    
    global ada    
    if ada:
        ada = False
        choose = Pesan.objects.aggregate(Max('pk'))['pk__max']

    obj = Pesan.objects.get(id=choose)
    ser_obj = serializers.serialize('json', [obj, ])
    data = json.loads(ser_obj)
    return JsonResponse(data, safe=False)

def asave(request):
    nama = request.GET['n']
    pesan = request.GET['m']
    user = ''
    if (request.user.is_authenticated):
        user = request.user.username
    if (nama == ''):
        hewan = random.choice(lst)
        lst.remove(hewan)
        new = Pesan(name="Anonymous " + hewan, message=pesan, awal=hewan[0], like=0, sender=user, date=datetime.date.today())
    else:
        new = Pesan(name=nama, message=pesan, awal=nama[0].upper(), like=0, sender=user, date=datetime.date.today())
    new.save()

    ind = Pesan.objects.aggregate(Max('pk'))['pk__max']
    obj = Pesan.objects.get(id=ind)
    ser_obj = serializers.serialize('json', [obj, ])
    data = json.loads(ser_obj)
    return JsonResponse(data, safe=False)

def like(request):
    nama = request.GET['n']
    pesan = request.GET['m']

    obj = Pesan.objects.get(name=nama, message=pesan)
    obj.like = obj.like + 1
    obj.save()

    ser_obj = serializers.serialize('json', [obj, ])
    data = json.loads(ser_obj)
    return JsonResponse(data, safe=False)