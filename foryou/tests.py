from django.test import TestCase, Client, LiveServerTestCase, tag
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.keys import Keys
from .views import index, read, send, history
from .models import Pesan
from .forms import Input_Form
import datetime, time

class ForyouUnitTest(TestCase):

    def test_main_page_url_is_exist_and_using_index_func(self):
        response = self.client.get('/foryou/')
        found = resolve('/foryou/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(found.func, index)

    def test_read_page_url_is_exist_and_using_read_func(self):
        response = self.client.get('/foryou/read/')
        found = resolve('/foryou/read/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(found.func, read)

    def test_send_page_url_is_exist_and_using_send_func(self):
        response = self.client.get('/foryou/send/')
        found = resolve('/foryou/send/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(found.func, send)
    
    def test_history_page_url_is_exist_and_using_history_func(self):
        response = self.client.get('/foryou/history/')
        found = resolve('/foryou/history/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(found.func, history)

    def test_create_model(self):
        new = Pesan.objects.create(name='Aa', message='main kuy', awal='A', like=0, sender="username", date=datetime.date.today())
        jumlah = Pesan.objects.all().count()
        self.assertEqual(jumlah, 1)

    def test_form_get_method(self):
        response = self.client.get('/foryou/send/save')
        form = Input_Form(response)
        self.assertEqual(response.status_code, 302)

    def test_form_post_method_anonymous(self):
        response = self.client.post('/foryou/send/save', {'name' : '', 'message' : 'hai', 'like': 0})
        jumlah = Pesan.objects.all().count()
        m = Pesan.objects.get(id=1)
        self.assertEqual(m.name.split()[0], 'Anonymous')
        self.assertEqual(m.message, 'hai')
        self.assertEqual(m.awal, m.name.split()[1][0])
        self.assertEqual(jumlah, 1)
        self.assertEqual(response.status_code, 200)

    def test_form_post_method_person(self):
        response = self.client.post('/foryou/send/save', {'name' : 'orang', 'message' : 'hai', 'like': 0})
        jumlah = Pesan.objects.all().count()
        m = Pesan.objects.get(id=1)
        self.assertEqual(m.name, 'orang')
        self.assertEqual(m.message, 'hai')
        self.assertEqual(m.awal, 'O')        
        self.assertEqual(jumlah, 1)
        self.assertEqual(response.status_code, 200)

    def test_form_post_method_authorized(self):
        response = self.client.post('/register/', data={'username': 'testuser', 'password1': 'hahahihi123', 'password2': 'hahahihi123'})
        response = self.client.post('/', {'username' : 'testuser', 'password' : 'hahahihi123'})
        response = self.client.post('/foryou/send/save', {'name' : 'orang', 'message' : 'hai', 'like': 0})
        m = Pesan.objects.get(id=1)
        self.assertIn('see your', response.content.decode('utf-8'))
        self.assertEqual(m.sender, 'testuser')

@tag('functional')
class ForYouFunctionalTest(LiveServerTestCase):

    def setUp(self):
        options = webdriver.chrome.options.Options()
        options.headless = True
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        self.selenium = webdriver.Chrome(options=options)
        
    def tearDown(self):
        self.selenium.quit()

    def testSaveMessageFromRead(self):
        self.selenium.get(f'{self.live_server_url}/foryou/read/')
        element = self.selenium.find_element_by_id("addButton")
        element.click()
        time.sleep(1)
        element = self.selenium.find_element_by_class_name("in_n")
        element.send_keys("orang")
        element = self.selenium.find_element_by_class_name("in_m2")
        element.send_keys("haloo")
        element = self.selenium.find_element_by_class_name("sendButton")
        element.click()
        time.sleep(1)
        self.assertIn("orang", self.selenium.page_source)
        self.assertIn("O", self.selenium.page_source)
        self.assertIn("haloo", self.selenium.page_source)

    def testSaveMessageFromSend(self):
        self.selenium.get(f'{self.live_server_url}/foryou/send/')
        element = self.selenium.find_element_by_class_name("in_n")
        element.send_keys("orang1")
        element = self.selenium.find_element_by_class_name("in_m")
        element.send_keys("haloo")
        element = self.selenium.find_element_by_id("sendReal")
        element.click()
        self.assertIn("orang1", self.selenium.page_source)
        self.assertIn("O", self.selenium.page_source)
        self.assertIn("haloo", self.selenium.page_source)
    
    def testLikeMessage(self):
        self.selenium.get(f'{self.live_server_url}/foryou/read/')
        element = self.selenium.find_element_by_id("addButton")
        element.click()
        time.sleep(1)
        element = self.selenium.find_element_by_class_name("in_n")
        element.send_keys("orang")
        element = self.selenium.find_element_by_class_name("in_m2")
        element.send_keys("haloo")
        element = self.selenium.find_element_by_class_name("sendButton")
        element.click()
        time.sleep(1)
        self.assertIn('<div class="myCount">0</div>', self.selenium.page_source)
        element = self.selenium.find_element_by_class_name("fa-heart-o")
        element.click()
        time.sleep(1)
        self.assertIn('<div class="myCount">1</div>', self.selenium.page_source)

    def testSaveMessageFromReadHasLogin(self):
        self.selenium.get(f'{self.live_server_url}/register/')
        element = self.selenium.find_element_by_id("id_username")
        element.send_keys("testuser")
        element = self.selenium.find_element_by_id("id_password1")
        element.send_keys("hahahihi123")
        element = self.selenium.find_element_by_id("id_password2")
        element.send_keys("hahahihi123")
        element = self.selenium.find_element_by_class_name("register-btn")
        element.click()

        self.selenium.get(f'{self.live_server_url}/')
        element = self.selenium.find_element_by_name("username")
        element.send_keys("testuser")
        element = self.selenium.find_element_by_name("password")
        element.send_keys("hahahihi123")
        element = self.selenium.find_element_by_class_name("register-btn")
        element.click()

        self.selenium.get(f'{self.live_server_url}/foryou/read/')
        element = self.selenium.find_element_by_id("addButton")
        element.click()
        time.sleep(1)
        element = self.selenium.find_element_by_class_name("in_n")
        element.send_keys("testnama")
        element = self.selenium.find_element_by_class_name("in_m2")
        element.send_keys("haloo")
        element = self.selenium.find_element_by_class_name("sendButton")
        element.click()
        time.sleep(1)
        m = Pesan.objects.get(name="testnama")
        self.assertEqual(m.sender, 'testuser')

    def testSaveMessageFromReadAnonymous(self):
        self.selenium.get(f'{self.live_server_url}/foryou/read/')
        element = self.selenium.find_element_by_id("addButton")
        element.click()
        time.sleep(1)
        element = self.selenium.find_element_by_class_name("in_m2")
        element.send_keys("haloo")
        element = self.selenium.find_element_by_class_name("sendButton")
        element.click()
        time.sleep(1)
        self.assertIn("Anonymous", self.selenium.page_source)
        self.assertIn("haloo", self.selenium.page_source)