# Staces Project

![pipeline](https://gitlab.com/fadlia68/tk2-ppw/badges/master/pipeline.svg)
![coverage](https://gitlab.com/fadlia68/tk2-ppw/badges/master/coverage.svg)

## Daftar isi

- [Anggota Kelompok](#anggota-kelompok)
- [Link Heroku App](#link-heroku-app)
- [Deskripsi Aplikasi](#deskripsi-aplikasi)
- [Daftar Fitur](#daftar-fitur)

## Anggota Kelompok

1. Annisa Dian Nugrahani (1906292906)
2. Ayuka Sonohata (1906298802)
3. Christian Raditya Restanto (1906292982)
4. Fadli Aulawi Al Ghiffari (1906285623)

## Link Heroku App

Link aplikasi kami: https://staces-app.herokuapp.com/

## Deskripsi Aplikasi

Aplikasi yang akan kami buat bernama STACES (Student Spaces). STACES merupakan aplikasi atau platform untuk memudahkan aktivitas dan kegiatan para pelajar atau mahasiswa di masa pandemi seperti ini. STACES memiliki banyak fitur yang sangat membantu dalam bentuk fitur untuk mengatur jadwal, fitur untuk mendapatkan pesan motivasi secara anonimus, fitur untuk sarana hiburan, dan juga fitur untuk mendapatkan sumber-sumber pelajaran.

## Daftar Fitur

1. TO DO

Fitur ini akan membantu orang-orang yang sedang melaksanakan WFH dalam mencatat tugas-tugas yang dilaksanakan nanti. Tugas-tugas ini nantinya akan dikelompokkan menjadi dua, yaitu tugas grup dan tugas personal. Tugas-Tugas itu akan diurutkan mulai dari deadline yang harinya lebih dahulu, kemudian jika terdapat kesamaan hari maka akan diurutkan berdasarkan jamnya. Fitur ini juga tersedia delete task, jadi ketika user sudah menyelesaikan tugasnya maka tugas yang ada di list tersebut akan dihilangkan.

2. Happy Space (Entertainment)

Fitur ini akan menjadi sebuah sarana hiburan untuk membantu orang-orang yang sedang melaksanakan WFH dalam merelaksasikan pikiran. Fitur ini menyediakan berbagai sumber hiburan untuk para pengunjung website dengan berbagai macam kategori, dimulai dari film, music, dan games.  Setiap category akan menyuguhkan berbagai informasi berbentuk artikel untuk menampilkan rekomendasi sesuai dengan kategori yang ada dan disertakan dengan deskripsi singkat.

3. Learning Space (Study Space)

Fitur ini akan memberikan rekomendasi platform yang dapat dijadikan sumber belajar selama WFH. Kami menyediakan berbagai sumber bidang pembelajaran yang dapat diakses, mulai dari programming, matematika, bahasa, bisnis, dan masih banyak lagi. Selain itu, kami juga menyediakan berbagai platform pembelajaran yang cukup terkenal disertai dengan deskripsi singkat yang tentu akan membuat pengunjung website kami memilih sesuai dengan keinginannya. Terakhir, kami juga menyediakan kontribusi untuk para pengunjung website kami jika ingin menambahkan sumber belajar yang tentunya akan menambah referensi untuk pengunjung website lainnya.

4. For You 

Fitur ini akan memberikan kesempatan bagi orang-orang yang sedang berada di rumah karena pandemi untuk tetap bersosialisasi dengan orang lain. Caranya adalah dengan mengirim sebuah pesan singkat dan akan diterima oleh user lain di aplikasi kami. Tentunya selain mengirim pesan, user juga bisa menerima pesan dari kiriman user lain. Meskipun pengirim dan penerima tidak tahu dengan siapa mereka berkomunikasi, pesan-pesan yang didapatkan bisa sangat berarti di masa pandemi ini.